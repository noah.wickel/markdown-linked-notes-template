# Markdown Linked Notes Template
This is a template for a collection of text files (e.g. notebooks, wikis, "Zettelkasten") located in a git repository. The following core features are desired:
- editing mainly in [Obsidian](https://wwww.obsidian.md), but any text editor possible
- asynchronous shared work possible, with note merging handled by git
- online viewing in GitLab possible, with working links between notes
	- this particularly means that Obsidians `[[ FILENAME ]]` wiki-type links are not supported
	- the plugin [Obsidian Link converter](https://github.com/ozntel/obsidian-link-converter) can be used to convert wikilinks to MdLinks
	- as a rule of thumb, avoid spaces in filenames if possible. If the file already has a space in it, the link must replace the space with `%20` like so: `[obsidian file](obsidian%20file.md)`

## Getting started
- if not yet installed, install git on your system from https://www.git-scm.com
- (create your repository in GitLab (new project))
- Clone this repository or the new one with `git clone`
	- if on Windows, use https (not ssh) in order to conveniently push with the Obsidian Git plugin (see below)
	- ![](images/01-clone.png)
- Open Obsidian and open the cloned folder as a new vault. If you already have a vault open, use the "Open another vault" button in the bottom left.
	- ![](images/02-opennew.png)  ![](images/03-open.png)]]
- Go to settings (`Ctrl+,`) and install the community plugins to ease your experience:
	- ![](images/[04-plugins.png)
	- Obsidian Git (install it)
		- ![](images/05-plugingit.png)
	- Obsidian Link Converter
		- same as above
	- after installation, the plugins must be turned on manually
		- Close the "Browse Plugins" window if still open
		- Flip the switch on both your newly installed plugins to enable them
		- ![](images/06-pluginon.png)
	- Find the community plugins area on the left side of the options menu:
		- ![](images/07-communityplugins.png)
		- Click one of them to open its settings. Change some things according to taste (e.g. enable automatic pulling when Obsidian is opened)
		- Personally, I don't like the automatic backup feature of Obsidian Git, as it will sometimes push my changes when I am in the middle of editing a document
- Set some hotkeys to Git pull, commit & push and for `Obsidian Link Converter: Vault: Links to Markdown`
- Create the gitignore file as below
- See more advice below
### .gitignore
The [.gitignore](.gitignore) file needs to contain the subfolder `.obsidian` in order not to push the personal settings for this vault. Obsidian cannot link to this file because it always assumes `.md` file endings, however in GitLab the link above should work.

## Test content
- This is a test link to another [file](otherfile.md)
- This is a test link to [itself](./README.md)
- This is a test link to a file with obsidian-style linking: [[obsidian file]]
	- git style link to the same file: [obsidian file](obsidian%20file.md) 
- This is a test link to a file in a subfolder called Topic A: [link](Topic A/newfile.md)
	- GitLab actually recognises this link correctly, however Obsidian terminates after the first space character
	- here is the test again without the space in the subfolder: [link](TopicA/newfile.md)
	- here is the test again with a repaired space in the subfolder: [link](Topic%20A/newfile.md)

This content is created to test/showcase the behaviour and rendering in GitLab online.

```mermaid
graph LR
f[first]
i[initial]
s[second]
t[third]

f & i --> s
s --> t
t --> s
```

### Git sync plugin in Obsidian
This text has been commited and pushed via the Obsidian git plugin. Keep in mind that on windows, this can only be done if the repository is cloned via HTTPS, not SSH cert.

##### Staying in sync
There are three actions to keep your local Obsidian folder in sync with the online Git repository: *Pull*, *Commit* and *Push*.
- **Pull**: ask the online Git repo if there are any new changes that you don't have yet
- **Push:** upload your local changes (which the online Git repository does not have yet)
	- a *Push* only works when you have at least one new, unpublished commit
- **Commit**: Gather all changes that you made since the last commit. Then, they can be pushed.

If you are unfamiliar with source control systems such as Git, it may seem strange to execute *two* actions (Commit, then Push) when you want to sync your work. However, there may be situations where you want to bundle some changes (maybe work on Topic A) into one commit and some other changes (work on Topic B) into another commit. These could later be reviewed independently.

For a convenient workflow, it is recommended to assign a keyboard shortcut to the `Obsidian Git` actions `Commit all changes`, `Push` and `Pull`. If you cannot memorize your shortcuts, press `Ctrl+P` and type `commit`, `push` or `pull` in the quick action bar, which will also show you the shortcuts if you had any assigned.

##### Merge conflicts
If there are changes to the same line of a file by two different users at the same time, there will be a merge conflict. That means, two versions exist and somebody must decide how to resolve the text back into one version with all the information in it.

This is what a conflict looks like:
```
<<<<<<< HEAD
This text is changed via Obsidian. At the same time, there is a change online.
=======
This text is added online. At the same time, there is a change in Obsidian.
>>>>>>> origin/main
```
When you pull and there are merge conflicts, the plugin will open an overview page with all the conflicts listed. You can correct them, then you should push all changes immediately. The overview page may look like [this](example_conflict-files-obsidian-git.md).

---