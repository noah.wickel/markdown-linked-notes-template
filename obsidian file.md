This file was created by linking to `[obsidian file](.md)`

- This is the same link to a file with obsidian-style linking: [README](README.md) converted to MdLinks by the plugin
- https://github.com/ozntel/obsidian-link-converter
